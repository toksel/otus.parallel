﻿using System;

namespace otus.parallel
{
    class ArrayGenerator
    {
        int[] _array;
        Random _rnd;
        public int[] Array => _array;
        public ArrayGenerator(int size)
        {
            _array = new int[size];
            _rnd = new Random();
            Init();
        }
        void Init()
        {
            for (int i = 0; i < _array.Length; i++)
            {
                _array[i] = _rnd.Next(1, 100);
            }
        }
    }
}
