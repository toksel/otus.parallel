﻿using System.Linq;
using System.Threading.Tasks;
using System;

namespace otus.parallel
{
    static class ArrayElementsSummizer
    {
        public static int ArraySumNonParallel(int[] array)
            => array.Sum();

        public static int ArraySumByTherad(int[] array)
        {
            int ret = 0;
            int taskCount = Environment.ProcessorCount;
            System.Console.WriteLine($"Logical core count: {taskCount}");
            int chunk = array.Length / taskCount;
            Task<int>[] tasks = new Task<int>[taskCount];
            for (int i = 0; i < taskCount; i++)
            {
                int[] chunkArray = array.Where(w => w >= i * chunk && w < (i + 1) * chunk).ToArray();
                tasks[i] = new Task<int>(() => ArraySumNonParallel(chunkArray));
                tasks[i].Start();
            }
            tasks.ToList().ForEach(f => ret += f.Result);
            return ret;
        }

        public static int ArraySumByParallel(int[] array)
            => array.AsParallel().Sum();
    }
}
