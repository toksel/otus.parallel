﻿using System;
using System.Collections.Generic;
using static otus.parallel.ArrayElementsSummizer;

namespace otus.parallel
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int[]> arrays = new List<int[]>{ new ArrayGenerator(100000).Array, new ArrayGenerator(1000000).Array, new ArrayGenerator(10000000).Array };
            TimePrinter printer = new();

            foreach (var array in arrays)
            {
                int cnt = array.Length;
                printer.PrintTime(ArraySumNonParallel, array, $"no Parallel for {cnt} elements");
                printer.PrintTime(ArraySumByTherad, array, $"byThread for {cnt} elements");
                printer.PrintTime(ArraySumByParallel, array, $"by Parallel for {cnt} elements");
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }

}
