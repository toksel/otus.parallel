﻿using System;
using System.Diagnostics;

namespace otus.parallel
{
    class TimePrinter
    {
        Stopwatch _sw;
        public TimePrinter()
        {
            _sw = new Stopwatch();
        }
        public void PrintTime(Func<int[], int> action, int[] array, string methodName)
        {
            _sw.Reset();
            Console.WriteLine($"Start {methodName}");
            _sw.Start();
            Console.WriteLine($"Result: {action.Invoke(array)}");
            _sw.Stop();
            Console.WriteLine($"Finished in: {_sw.ElapsedMilliseconds} ms");
        }
    }
}
